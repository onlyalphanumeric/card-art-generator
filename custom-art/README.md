## Card Art Design Requirements

### Design Topics
Each card list specified [here](./example/archetype-list.png) will need to have a custom art design. The current set of art are pulled from various locations off the internet and are subject to DCMA.

### Dimensions:
The dimension of the completed playing card will be 2.5 inches tall by 1.5 inches wide. A skeleton design of [dragonite-alpha](./example/demo-dragonite-alpha.png) is provided as a reference for the relative locations of various texts, images, and descriptions.

For the art specifically: The card art will rest at the top left corner and should be of dimension 700x310 pixels. Art can be made with higher dimensions but should maintain the same aspect ratio and be a minimum of 700x310.

**Note: the design of the demo card is also a stub implementation and could use iterations**
